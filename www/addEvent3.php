<?php 
session_start();
if($_SESSION['status']!='admin')
{
	$_SESSION['status']='';
   header('location: login.php?typ=4');
}
 ?>			
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Add an event</title>
<link rel="stylesheet" type="text/css" href="home.css" >
<link rel="stylesheet" type="text/css" href="printStyle.css" media="print"  />
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationTextarea.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationTextarea.css" rel="stylesheet" type="text/css" />
</head>
<?php include("adminPart1.php"); ?>
<form action="processEvent3.php" method="post" enctype="multipart/form-data" name="form1" id="form1">
  <table class="form" width="833" border="0">
    <tr>
      <td width="147">&nbsp;</td>
      <td width="97">&nbsp;</td>
      <td width="575">&nbsp;</td>
    </tr>
    <tr>
      <td>Title</td>
      <td>&nbsp;</td>
      <td><span id="sprytextfield1">
        <input type="text" name="title" id="title" accesskey="title" />
        <span class="textfieldRequiredMsg">A value is required.</span></span></td>
    </tr>
    <tr>
      <td>Date</td>
      <td>&nbsp;</td>
      <td><span id="sprytextfield2">
      <input type="text" name="date" id="date" accesskey="date" />
      <span class="textfieldRequiredMsg">A value is required.</span><span class="textfieldInvalidFormatMsg">Invalid format.</span></span></td>
    </tr>
    <tr>
      <td>Summary</td>
      <td>&nbsp;</td>
      <td><span id="sprytextarea1">
      <textarea name="summary" id="summary" cols="45" rows="5" accesskey="summary"></textarea>
      <span id="countsprytextarea1">&nbsp;</span><span class="textareaRequiredMsg">A value is required.</span><span class="textareaMaxCharsMsg">Exceeded maximum number of characters.</span></span></td>
    </tr>
    <tr>
      <td>Description</td>
      <td>&nbsp;</td>
      <td><span id="sprytextarea2">
      <textarea name="description" id="description" cols="45" rows="5" accesskey="description"></textarea>
      <span id="countsprytextarea2">&nbsp;</span><span class="textareaRequiredMsg">A value is required.</span><span class="textareaMaxCharsMsg">Exceeded maximum number of characters.</span></span></td>
    </tr>
    <tr>
      <td>Add Picture</td>
      <td>&nbsp;</td>
      <td><input type="file" name="picture1" id="picture1" accesskey="picture1" /></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td><input type="file" name="picture2" id="picture2" accesskey="picture2" /></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td><input type="file" name="picture3" id="picture3" accesskey="picture3" /></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td><input type="file" name="picture4" id="picture4" accesskey="picture4" /></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td><input type="file" name="picture5" id="picture5" accesskey="picture5" /></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td><input type="submit" name="submit" id="submit" value="Submit" accesskey="submit" />
        <input type="reset" name="reset" id="reset" value="Reset" accesskey="reset" /></td>
      </tr>
  </table>
</form>
<?php include("adminPart2.php"); ?>
<script type="text/javascript">
<!--
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1", "none", {validateOn:["blur"]});
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2", "date", {format:"yyyy-mm-dd", hint:"yyyy-mm-dd", validateOn:["blur"]});
var sprytextarea1 = new Spry.Widget.ValidationTextarea("sprytextarea1", {counterId:"countsprytextarea1", validateOn:["blur"], counterType:"chars_remaining", maxChars:250, hint:"Summary of the event"});
var sprytextarea2 = new Spry.Widget.ValidationTextarea("sprytextarea2", {maxChars:750, counterId:"countsprytextarea2", hint:"Short description", counterType:"chars_remaining"});
//-->
</script>
</body>
</html>
<?php 
session_start();
if($_SESSION['status']!='admin')
{
	$_SESSION['status']='';
   header('location: login.php?typ=4');
}
 ?>			
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link rel="stylesheet" type="text/css" href="home.css" >
<link rel="stylesheet" type="text/css" href="printStyle.css" media="print"  />
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationTextarea.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationTextarea.css" rel="stylesheet" type="text/css" />
</head>
<body>
<?php
include("adminPart1.php");

    ?>
<form action="processNotice.php" method="post" enctype="multipart/form-data" name="changer">
  <table class="form" width="1000" border="0">
    <tr>
      <td colspan="3">Add new Notice</td>
    </tr>
    <tr>
      <td width="295">Title</td>
      <td width="561"><span id="sprytextfield1">
        <input type="text" name="title" id="title" accesskey="title" />
      <span class="textfieldRequiredMsg">A value is required.</span></span></td>
      <td width="130"><input name="MAX_FILE_SIZE" value="1024000" type="hidden"></td>
    </tr>
    <tr>
      <td>Summary</td>
      <td><span id="sprytextarea1">
      <textarea name="summary" id="summary" cols="45" rows="5" accesskey="summary"></textarea>
      <span id="countsprytextarea1">&nbsp;</span><span class="textareaRequiredMsg">A value is required.</span><span class="textareaMaxCharsMsg">Exceeded maximum number of characters.</span></span></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>Notice </td>
      <td><input name="notice" accept="image/jpeg" type="file"></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input type="submit" name="submit" id="submit" value="Submit" accesskey="submit" />  <input type="reset" name="reset" id="reset" value="Reset" accesskey="reset" /></td>
      <td>&nbsp;</td>
    </tr>
  </table>
</form>
<script type="text/javascript">
<!--
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1", "none", {validateOn:["blur"]});
var sprytextarea1 = new Spry.Widget.ValidationTextarea("sprytextarea1", {maxChars:200, counterType:"chars_remaining", counterId:"countsprytextarea1", validateOn:["blur"]});
//-->
</script>
<?php include("adminPart2.php"); ?>
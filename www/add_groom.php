<?php 
session_start();
if($_SESSION['status']!='admin')
{
	$_SESSION['status']='';
   header('location: login.php?typ=4');
}
 ?>			
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link rel="stylesheet" type="text/css" href="home.css" >
<link rel="stylesheet" type="text/css" href="printStyle.css" media="print"  />
</head>
<?php include("adminPart1.php")?>
<html>
<head>
<title>
Room </title>
<script src="SpryAssets/SpryValidationRadio.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationRadio.css" rel="stylesheet" type="text/css" />
</head>
<body>
<form action="groom_details.php" method="post">
<table>
  <tr><td><div align="left">Room no</div></td><td><div align="left">
    <input type="text" name="room" />
  </div></td><td></td></tr>
  <tr><td><div align="left">Number of beds</div></td><td><div align="left">
    <input type="text" name="bed" />
  </div></td><td></td></tr>
  <tr><td>Number of tables</td><td><div align="left">
    <input type="text" name="table" />
  </div></td><td></td></tr>
  <tr><td><div align="left">available</div></td><td><div id="spryradio1">
    <table width="155" height="28">
      <tr>
        <td width="64"><label>
          <input type="radio" name="av" value="yes" id="av_0" />
          Yes</label></td>
        <td width="79"><label>
          <input type="radio" name="av" value="no" id="av_1" />
          No</label></td>
      
    </table>
    </tr>
      <tr><td></td><td><input type="submit" value="submit" /></td><td></td></tr>
  <span class="radioRequiredMsg">Please make a selection.</span></div></td><td></td></tr>
</table>
</form>

<script type="text/javascript">
var spryradio1 = new Spry.Widget.ValidationRadio("spryradio1");
</script>
<?php include("adminPart2.php"); ?>
</body>
</html>
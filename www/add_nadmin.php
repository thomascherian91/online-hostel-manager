<?php 
session_start();
if($_SESSION['status']!='admin')
{
	$_SESSION['status']='';
   header('location: login.php?typ=4');
}
 ?>			
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link rel="stylesheet" type="text/css" href="home.css" >
<link rel="stylesheet" type="text/css" href="printStyle.css" media="print"  />
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
</head>
<?php include("adminPart1.php"); ?>

    <div align="left">Manage Admin<br /><br /></div>
  <form action="admin_details.php" method="post" enctype="multipart/form-data">
   <div align="center">
    <table width="367" border="0">
      <tr>
        <td width="139" height="50"><div align="left">Admin id</div></td>
        <td width="172"><div align="left"><span id="sprytextfield1">
        <input type="text" name="id" id="id" />
        <span class="textfieldRequiredMsg">A value is required.</span><span class="textfieldMinCharsMsg">Minimum number of characters not met.</span></span></div></td>
      </tr>
      
      
      <tr>
        <td height="50"><div align="left">Name</div></td>
        <td>
          <div align="left"><span id="sprytextfield2">
            <input type="text" name="name" id="name" />
          <span class="textfieldRequiredMsg">A value is required.</span></span></div></td>
      </tr>
      
      
      <tr>
        <td height="50"><div align="left">Designation</div></td>
        <td><div align="left"><span id="sprytextfield3">
          <input type="text" name="desgn" id="desgn" />
        <span class="textfieldRequiredMsg">A value is required.</span></span></div></td>
      </tr>
      
      <tr>
      <td height="50"><div align="left">Add Picture</div></td>
      <td><input type="file" name="picture" id="picture" accesskey="picture" /></td>
    </tr>
      
      <tr>
        <td height="50"><div align="left">Contact number</div></td>
        <td><div align="left"><span id="sprytextfield4">
          <input type="text" name="cntct" id="cntct" />
        <span class="textfieldRequiredMsg">A value is required.</span></span></div></td>
      </tr>
      
      <tr>
        <td>&nbsp;</td>
        <td><input type="submit" name="button" id="button" value="Submit" /></td>
      </tr>
     </table></div>
      <?php include("adminPart2.php"); ?>
</body>
<script type="text/javascript">
<!--
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1", "none", {validateOn:["blur"], minChars:2});
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2", "none", {validateOn:["blur"]});
var sprytextfield3 = new Spry.Widget.ValidationTextField("sprytextfield3");
var sprytextfield4 = new Spry.Widget.ValidationTextField("sprytextfield4", "none", {validateOn:["blur"]});
//-->
</script>
</html>
    
<?php 
session_start();
if($_SESSION['status']!='admin')
{
	$_SESSION['status']='';
   header('location: login.php?typ=4');
}
include("connectDB.php");
 ?>	

 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link rel="stylesheet" type="text/css" href="home.css" >
<script src="SpryAssets/SpryCollapsiblePanel.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<link href="SpryAssets/SpryCollapsiblePanel.css" rel="stylesheet" type="text/css">
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
</head>
<body>
<?php include("adminPart1.php");
?>
    <table width="1091" height="488" border="0">
      <tr>
        <td width="221" height="106">&nbsp;</td>
        <td width="617"><div align="center"><strong>MESS BILL GENERATOR</strong></div></td>
        <td width="239">&nbsp;</td>
      </tr>
      <tr>
        <td height="315">&nbsp;</td>
        <td><div id="CollapsiblePanel1" class="CollapsiblePanel">
          <div class="CollapsiblePanelTab" tabindex="0">Enter the month for which Mess Bill is to be generated:</div>
          <div class="CollapsiblePanelContent">
            <table width="616" border="4">
              <tr>
                <td>MONTH</td>
                <td>
                <form name="form1" method="post" action="calculation.php">
                  <label for="month"></label>
                  <select name="month" id="month" accesskey="month">
                    <option>January</option>
                    <option>February</option>
                    <option>March</option>
                    <option>April</option>
                    <option>May</option>
                    <option>June</option>
                    <option>July</option>
                    <option>August</option>
                    <option>September</option>
                    <option>October</option>
                    <option>November</option>
                    <option>December</option>
                  </select>
                </td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>
                  <input type="submit" name="sub" id="sub" value="GENERATE" accesskey="sub">
                </form></td>
              </tr>
            </table>
            <span id="sprytextfield1">          </span></div>
        </div>
          <p>&nbsp;</p>
          <p>&nbsp;</p></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td height="59">&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table>
<?php include("adminPart2.php");
?>
</body>
</html>
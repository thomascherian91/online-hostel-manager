<?php 
session_start();
if($_SESSION['status']!='admin')
{
	$_SESSION['status']='';
   header('location: login.php?typ=4');
}
 ?>	
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Customize Hostel</title>
<link rel="stylesheet" type="text/css" href="home.css" >
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationTextarea.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationTextarea.css" rel="stylesheet" type="text/css" />
</head>
<?php
//include("connectDB.php");
include("adminPart1.php");
$query="select * from customize";
$result=mysql_query($query) or die(mysql_error());
if($row=mysql_fetch_array($result))
{
	echo "Hostel is already customized as follows...";
	?>
 <table class="form">
 <tr><td>&nbsp;</td>
 <td>&nbsp;</td>
 <td>&nbsp;</td>
 </tr>
 <tr>
   <td>Hostel Name</td>
   <td>&nbsp;</td>
   <td><?php echo $row['hostel_name']; ?></td>
 </tr>
 <tr>
   <td>Address</td>
   <td>&nbsp;</td>
   <td><?php echo $row['address']; ?></td>
   </tr>
 <tr>
   <td>Hostel Picture</td>
   <td>&nbsp;</td>
   <td><img src="dispImage.php?num=1&amp;table=customize" alt="Hostel Pic" name="hostelPic" width="249" height="105" id="hostelPic" /></td>
   </tr>
 </table>
 <?php
}
?>
<p><strong>Customize Hostel
</strong></p>
<form action="customize.php" method="post" enctype="multipart/form-data" name="form1" id="form1">
  <table class="form" width="757" height="83" border="0">
    <tr>
      <td width="124">Name:</td>
      <td width="623"><span id="sprytextfield1">
        <input type="text" name="hostel_name" id="hostel_name" accesskey="hostel_name" />
      <span class="textfieldRequiredMsg">A value is required.</span></span></td>
    </tr>
    <tr>
      <td>Address:</td>
      <td><span id="sprytextarea1">
        <textarea name="hostel_address" id="hostel_address" cols="45" rows="5" accesskey="hostel_address"></textarea>
      <span class="textareaRequiredMsg">A value is required.</span></span></td>
    </tr>
    <tr>
      <td>Picture:</td>
      <td><label>
        <input type="file" name="hostel_pic" id="hostel_pic" accesskey="hostel_pic" />
      </label></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input type="submit" name="cust_submit" id="cust_submit" value="Submit" accesskey="cust_submit" />   <input type="reset" name="cust_reset" id="cust_reset" value="Reset" accesskey="cust_reset" /></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
</form>
<p>&nbsp;</p>
<?php include("adminPart2.php"); ?>
<script type="text/javascript">
<!--
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1", "none", {validateOn:["blur"]});
var sprytextarea1 = new Spry.Widget.ValidationTextarea("sprytextarea1");
//-->
</script>
</html>
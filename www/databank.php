<?php 
session_start();
if($_SESSION['status']!='admin')
{
	$_SESSION['status']='';
   header('location: login.php?typ=4');
}
 ?>			
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link rel="stylesheet" type="text/css" href="home.css" >
<link rel="stylesheet" type="text/css" href="printStyle.css" media="print"  />
<script src="SpryAssets/SpryCollapsiblePanel.js" type="text/javascript"></script>
<link href="SpryAssets/SpryCollapsiblePanel.css" rel="stylesheet" type="text/css">
</head>
<?php include("adminPart1.php");?>


<h2>DATA BANK</h2>
<table width="905">
<tr>
<td width="119" height="58"></td>
<td width="158"><div id="CollapsiblePanel1" class="CollapsiblePanel">
  <div class="CollapsiblePanelTab" tabindex="0">Student</div>
  <div class="CollapsiblePanelContent">
    <p style="font-size:14px"><a href="st_details.php">Add student</a></p>
    <p style="font-size:14px"><a href="delet.php">Delete student</a></p>
    <p style="font-size:14px"><a href="edit1.php" class="CollapsiblePanelContent">Edit student</a></p>
  </div>
</div></td><td width="394"></td>
<td width="214"><div align="justify"><a href="blood.php"><strong>Blood bank</strong></a></div></td>
</tr>
<tr><td width="119" height="55"></td>
<td width="158"><div id="CollapsiblePanel2" class="CollapsiblePanel">
  <div class="CollapsiblePanelTab" tabindex="0">Staff</div>
  <div class="CollapsiblePanelContent">
    <p style="font-size:14px"><a href="staff_details.php">Add staff</a></p>
    <p style="font-size:14px"><a href="delet_staff.php">Delete staff</a></p>
    <p style="font-size:14px"><a href="edit2.php">Edit Staff</a></p>
  </div>
</div></td><td></td>
<td><div align="justify"><a href="vehicle.php"><strong>Vehicle log</strong></a></div></td>
</tr>
<tr><td width="119" height="61"></td><td width="158"><div id="CollapsiblePanel3" class="CollapsiblePanel">
  <div class="CollapsiblePanelTab" tabindex="0">Guest</div>
  <div class="CollapsiblePanelContent">
    <p style="font-size:14px"><a href="guest_details.php">Add guest</a></p>
    <p style="font-size:14px"><a href="delet_guest.php">Delete guest</a></p>
    <p style="font-size:14px"><a href="edit3.php">Edit guest</a></p>
  </div>
</div></td><td></td>
<td><div align="justify"><a href="laptop.php"><strong><u>Laptops</u></strong></a></div></td>
</tr>
</table>
<script type="text/javascript">
var CollapsiblePanel1 = new Spry.Widget.CollapsiblePanel("CollapsiblePanel1", {contentIsOpen:false});
var CollapsiblePanel2 = new Spry.Widget.CollapsiblePanel("CollapsiblePanel2", {contentIsOpen:false});
var CollapsiblePanel3 = new Spry.Widget.CollapsiblePanel("CollapsiblePanel3", {contentIsOpen:false});
</script>
<?php include("adminPart2.php"); ?>
</body>
</html>
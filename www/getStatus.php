<?php 
session_start();
if($_SESSION['status']!='admin')
{
	$_SESSION['status']='';
   header('location: login.php?typ=4');
}
 ?>			
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Status Archives</title>
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="home.css" >
<link rel="stylesheet" type="text/css" href="printStyle.css" media="print"  />
</head>

<body>
<?php include("adminPart1.php"); ?>
<p>Status Archives </p>
<form id="form1" name="form1" method="post" action="getStatus.php">
  <table width="656" border="0">
    <tr>
      <td width="196">Date</td>
      <td width="48">&nbsp;</td>
      <td width="398"><span id="sprytextfield1">
      <input type="text" name="reqdate" id="reqdate" accesskey="reqdate" />
      <span class="textfieldRequiredMsg">A value is required.</span><span class="textfieldInvalidFormatMsg">Invalid format.</span></span></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td><input type="submit" name="submit" id="submit" value="Submit" accesskey="submit" /></td>
    </tr>
  </table>
</form>
<?php 
if(isset($_POST['submit']))
{
	$date=$_POST['reqdate'];
	$checkquery="select * from status where date='$date'";
	$resultquery=mysql_query($checkquery)or die(mysql_error());
	if($checkrow=mysql_fetch_array($resultquery))
	{
?>
<p>&nbsp;</p>
<h3>Status List for 
<?php echo $date; ?> generated at <?php echo $checkrow['time']; ?>
</h3></br>
<table width="801" border="1">
  <tr>
    <td width="131">User ID</td>
    <td width="121">Room No</td>
    <td width="315">Name</td>
    <td width="206">Status</td>
  </tr>
<?php
$query="select * from student_status where date='$date'";
$result=mysql_query($query)or die(mysql_error());
while($row=mysql_fetch_array($result))
{
	$id=$row['uid'];
	$querystud="select name,room_no from student where uid='$id'";
	$resultstud=mysql_query($querystud) or die(mysql_error());
	$rowstud=mysql_fetch_array($resultstud);
?>
<tr>
	<td width="131"><?php echo $row['uid']; ?></td>
    <td width="121"><?php echo $rowstud['room_no']; ?></td>
    <td width="315"><?php echo $rowstud['name']; ?></td>
    <td width="206"><?php $status=$row['status'];
	if($status=='in')echo "Inside hostel, without Permission";
	elseif($status=='ip')echo "Inside hostel, with Permission";
	elseif($status=='op')echo "Outside hostel, with Permission";
	elseif($status=='on')echo "Outside hostel, without Permission";
	elseif($status=='wc')echo "Went to college"; ?>
	</td>
  </tr>
 <?php } ?>
 </table>
<?php }
	else
	{
		echo "<b style='color:red;'>No data found for the requested date...try another date..";
	}
}
include("adminPart2.php"); ?>

<script type="text/javascript">
<!--
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1", "date", {format:"yyyy-mm-dd", hint:"yyyy-mm-dd", validateOn:["blur"]});
//-->
</script>
</body>
</html>
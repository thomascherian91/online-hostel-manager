<?php 
session_start();
if(!isset($_SESSION['status'])or $_SESSION['status']=='user')
   header('location: logout.php');
 ?>	
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Customize Hostel</title>
<link rel="stylesheet" type="text/css" href="home.css" >
<script src="SpryAssets/SpryValidationTextarea.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationTextarea.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
</head>
<body>

<table width="1330" height="855" border="0" padding="0">
  <tr>
    <td height="60" colspan="3"><table class="header" width="1322" height="93" border="0" > 
      <tr>
        <td class="title" colspan="2" rowspan="2">Online Hostel Manager</td>  
        <td width="148" height="46">&nbsp;</td>
        <td width="139">&nbsp;</td>
        <td width="106">&nbsp;</td>
        <td width="244" rowspan="2">&nbsp;</td>
      </tr> 
      <tr class="noprint" >
        <td height="41"><a href="logout.php">Sign Out</a></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td class="noprint" width="156" rowspan="2"><table class="noprint" width="156" height="635" border="0" padding="0">
      <tr>
        <td width="150" height="152">&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td height="68">&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
    </table></td>
    <td valign="top" width="848" rowspan="2"><p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
<?php
include("connectDB.php");
$query="select * from customize";
$result=mysql_query($query) or die(mysql_error());
if($row=mysql_fetch_array($result))
{
	echo "Hostel is already customized as follows...";
	?>
 <table class="form">
 <tr><td>&nbsp;</td>
 <td>&nbsp;</td>
 <td>&nbsp;</td>
 </tr>
 <tr>
   <td>Hostel Name</td>
   <td>&nbsp;</td>
   <td><?php echo $row['hostel_name']; ?></td>
 </tr>
 <tr>
   <td>Address</td>
   <td>&nbsp;</td>
   <td><?php echo $row['address']; ?></td>
   </tr>
 <tr>
   <td>Hostel Picture</td>
   <td>&nbsp;</td>
   <td><img src="dispImage.php?num=1&amp;table=customize" alt="Hostel Pic" name="hostelPic" width="249" height="105" id="hostelPic" /></td>
   </tr>
 </table>
 <?php
}
?>
<p style="color:#F00;"><strong>Your Hostel Manager is not Customized. Customize the system before continuing...
</strong></p>
<form action="customize.php" method="post" enctype="multipart/form-data" name="form1" id="form1">
  <table class="form" width="757" height="83" border="0">
    <tr>
      <td width="124">Name:</td>
      <td width="623"><span id="sprytextfield1">
        <input type="text" name="hostel_name" id="hostel_name" accesskey="hostel_name" />
        <span class="textfieldRequiredMsg">A value is required.</span></span></td>
    </tr>
    <tr>
      <td>Address:</td>
      <td><span id="sprytextarea1">
        <textarea name="hostel_address" id="hostel_address" cols="45" rows="5" accesskey="hostel_address"></textarea>
        <span class="textareaRequiredMsg">A value is required.</span></span></td>
    </tr>
    <tr>
      <td>Picture:</td>
      <td><label>
        <input type="file" name="hostel_pic" id="hostel_pic" accesskey="hostel_pic" />
      </label></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input type="submit" name="cust_submit" id="cust_submit" value="Submit" accesskey="cust_submit" />   <input type="reset" name="cust_reset" id="cust_reset" value="Reset" accesskey="cust_reset" /></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
</form>
  <p>&nbsp;</p>
      <p><br />
        
        
        <body background="bg.jpg">
      </p></td>
    <td class="noprint" width="306" height="102">&nbsp;</td>
  </tr>
  <tr>
    <td class="noprint" height="428">&nbsp;</td>
  </tr>
  <tr>
    <td height="91" colspan="3"><table class="header" width="1342" border="0">
      <tr class="noprint">
        <td width="1166" class="foot" id="bottom">Supported by Online Hostel Manager</td>
      </tr>
      <tr>
        <td class="credits" id="credits">Thomas Cherian, Thomas J Padamadan, Sonu Wilson</td>
      </tr>
    </table></td>
  </tr>
</table>
<script type="text/javascript">
<!--
var sprytextarea1 = new Spry.Widget.ValidationTextarea("sprytextarea1", {validateOn:["blur"]});
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1", "none", {validateOn:["blur"]});
//-->
</script>
</body>
</html>
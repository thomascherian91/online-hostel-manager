<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>login</title>
<link rel="stylesheet" type="text/css" href="home.css" >
<link rel="stylesheet" type="text/css" href="printStyle.css" media="print"  />
<script src="SpryAssets/SpryCollapsiblePanel.js" type="text/javascript"></script>
<link href="SpryAssets/SpryCollapsiblePanel.css" rel="stylesheet" type="text/css" />
</head>

<body>

<table width="1330" height="855" border="0" padding="0">
  <tr>
    <td height="60" colspan="3"><table class="header" width="1322" height="93" border="0" > 
      <tr>
        <td class="title" colspan="2" rowspan="2">Online Hostel Manager</td>  
        <td width="148" height="46">&nbsp;</td>
        <td width="139">&nbsp;</td>
        <td width="106">&nbsp;</td>
        <td width="244" rowspan="2">&nbsp;</td>
      </tr> 
      <tr class="noprint" >
        <td height="41">&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td class="noprint" width="156" rowspan="2"><table class="noprint" width="156" height="635" border="0" padding="0">
      <tr>
        <td width="150" height="152">&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td height="68">&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
    </table></td>
    <td valign="top" width="848" rowspan="2"><p>&nbsp;</p>
      <p style="color:#F00; font-weight:bold;"><?php 
	  if(isset($_GET['typ']))
	  {
		  $typ=$_GET['typ'];
		  if($typ==1)
		  	echo "Hostel Manager is not customized...You are logged out...";
		if($typ==2)
			echo "Incorrect UserId or Password!!!";
		if($typ==3)
			echo "Thank you...You are logged out...";
		if($typ==4)
			echo "You are not permitted to open an Admin page...login as Admin to proceed...";
		if($typ==5)
			echo "You are not permitted to open a User page...Login as 'user' to proceed...";
		if($typ==6)
			echo "You are not permitted to open a guest page...Login as 'Guest' to proceed...";
	  }
	  ?>
	  </p>
      <p>&nbsp;</p>
      <div id="CollapsiblePanel1" class="CollapsiblePanel">
        <div class="CollapsiblePanelTab" tabindex="0">Login Here</div>
        <div class="CollapsiblePanelContent">
          <form id="form1" name="form1" method="post" action="verify.php">
            <table width="382" height="163" border="0">
              <tr>
                <td width="150">User ID</td>
                <td width="222"><label>
                  <input type="text" name="userid" id="userid" accesskey="userid" />
                </label></td>
              </tr>
              <tr>
                <td>Password</td>
                <td><label>
                  <input type="password" name="pword" id="pword" accesskey="pword" />
                </label></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td><label>
                  <input type="submit" name="submit" id="submit" value="Submit" accesskey="submit" />
                  <input type="reset" name="reset" id="reset" value="Reset" accesskey="reset" />
                </label></td>
              </tr>
            </table>
          </form>
        </div>
      </div>
      <p>&nbsp;</p>
      <p><br />
        
        
        <body background="bg.jpg">
      </p></td>
    <td class="noprint" width="306" height="102">&nbsp;</td>
  </tr>
  <tr>
    <td class="noprint" height="428">&nbsp;</td>
  </tr>
  <tr>
    <td height="91" colspan="3"><table class="header" width="1342" border="0">
      <tr class="noprint">
        <td width="1166" class="foot" id="bottom">Supported by Online Hostel Manager</td>
      </tr>
      <tr>
        <td class="credits" id="credits">Thomas Cherian, Thomas J Padamadan, Sonu Wilson</td>
      </tr>
    </table></td>
  </tr>
</table>
<script type="text/javascript">
<!--
var CollapsiblePanel1 = new Spry.Widget.CollapsiblePanel("CollapsiblePanel1", {contentIsOpen:false});
//-->
</script>
</body>
</html>
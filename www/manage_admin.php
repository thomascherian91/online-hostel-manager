<?php 
session_start();
if($_SESSION['status']!='admin')
{
	$_SESSION['status']='';
   header('location: login.php?typ=4');
}
 ?>			
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link rel="stylesheet" type="text/css" href="home.css" >
<link rel="stylesheet" type="text/css" href="printStyle.css" media="print"  />
</head>
<?php include("adminPart1.php"); ?>
    <div align="left">Admin manager </div>
    <div align="center">
    <table width="472" border="0">
      <tr>
        <td width="126" height="34"><div align="left"><a href="add_nadmin.php">Add new admin</a></div></td>
        <td width="336"><div align="right"><a href="admin_list.php">List of administrators</a></div></td>
      </tr>
      <tr>
        <td height="62"><div align="left"><a href="delete_admin.php">Delete admin</a></div></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td height="28"><div align="left"><a href="edit_admin.php">Edit admin</a></div></td>
        <td>&nbsp;</td>
      </tr>
    </table></div>
<?php include("adminPart2.php");
?>
</html>
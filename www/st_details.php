<?php 
session_start();
if($_SESSION['status']!='admin')
{
	$_SESSION['status']='';
   header('location: login.php?typ=4');
}
 ?>			
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link rel="stylesheet" type="text/css" href="home.css" >
<link rel="stylesheet" type="text/css" href="printStyle.css" media="print"  />
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationTextarea.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
<link href="SpryAssets/SpryValidationTextarea.css" rel="stylesheet" type="text/css">
</head>
<?php  include("adminPart1.php"); ?>

<h1>Student details</h1>
<form action="add_student.php" method="post" enctype="multipart/form-data">
<table>

<tr><td><div align="left">Student id            </div></td><td><div align="left"><span id="sprytextfield1"><span id="sprytextfield4">
  <input type="text" name="sid" id="sid" accesskey="sid">
  <span class="textfieldRequiredMsg">A value is required.</span><span class="textfieldMaxCharsMsg">Exceeded maximum number of characters.</span></span>
</span></div></td></tr>


<tr><td><div align="left">Name                   </div></td><td><div align="left"><span id="sprytextfield2">
  <input type="text" name="fname" id="fname" accesskey="fname">
  <span class="textfieldRequiredMsg">A value is required.</span></span></div></td></tr>

<tr><td><div align="left">Date of birth         </div></td><td><div align="left">
  <input type="text" name="dob" id="dob" accesskey="dob" />
</div></td></tr>

<tr><td><div align="left">Date of admission     </div></td><td><div align="left">
  <input type="text" name="adate" >
</div></td></tr>

<tr><td><div align="left">current address       </div></td><td><div align="left"><span id="sprytextarea2">
<textarea name="caddres" id="caddres" cols="30" rows="5" accesskey="caddres"></textarea>
<span class="textareaRequiredMsg">A value is required.</span><span class="textareaMaxCharsMsg">Exceeded maximum number of characters.</span></span></div></td></tr>

<tr><td><div align="left">permanant address     </div></td><td><div align="left"><span id="sprytextarea1">
  <textarea name="paddres" id="paddres" cols="30" rows="5" accesskey="paddres"></textarea>
  <span class="textareaRequiredMsg">A value is required.</span><span class="textareaMaxCharsMsg">Exceeded maximum number of characters.</span></span></div></td></tr>

<tr><td><div align="left">course of study       </div></td><td><div align="left">
  <select name="course" id="course" accesskey="course">
    <option>B.tech</option>
    <option>M.tech</option>
    <option>MBA</option>
    <option>Others</option>
  </select>
</div></td></tr>

<tr><td><div align="left">branch                 </div></td><td><div align="left">
  <select name="branch" size="1" id="branch" accesskey="branch">
    <option>AEI</option>
    <option>CSE</option>
    <option>ECE</option>
    <option>EEE</option>
    <option>IT</option>
    <option>Others</option>
  </select>
</div></td></tr>

<tr>
  <td><div align="left">Semester</div></td><td><div align="left">
    <label for="sem"></label>
    <select name="sem" id="sem" accesskey="sem">
      <option>S1&amp;S2</option>
      <option>S3</option>
      <option>S4</option>
      <option>S5</option>
      <option>S6</option>
      <option>S7</option>
      <option>S8</option>
    </select>
  </div></td></tr>

<tr>
  <td><div align="left">Blood group</div></td><td><div align="left">
    <select name="bg" id="bg">
      <option>A+</option>
      <option>A-</option>
      <option>B+</option>
      <option>B-</option>
      <option>AB+</option>
      <option>AB-</option>
      <option>O+</option>
      <option>O-</option>
    </select>
  </div></td></tr>

<tr><td><div align="left">Do you have laptop</div></td><td><div align="left">
  <input type="radio" name="lp" id="lp1" value="yes" />
  YES  <input type="radio" name="lp" id="lp2" value="no" />NO</div></td></tr>

<tr><td><div align="left">Do you have vehicle</div></td><td><div align="left"> <input type="radio" name="vh" id="vh1" value="yes" />
  YES  <input type="radio" name="vh" id="vh2" value="no" />NO</div></td></tr>

<tr><td><div align="left">Course year       </div></td><td><div align="left">
  <input type="text area" name="ycourse" >
</div></td></tr>

<tr>
      <td><div align="left">Add Picture</div></td>
      <td><input type="file" name="picture" id="picture" accesskey="picture" /></td>
    </tr>

<tr><td><div align="left">Email ID </div></td><td><div align="left"><span id="sprytextfield5">
  <input type="text" name="email" id="email">
  <span class="textfieldRequiredMsg">A value is required.</span><span class="textfieldInvalidFormatMsg">Invalid format.</span></span></div></td></tr>

<tr><td><div align="left">Parents contact nuber </div></td><td><div align="left">
  <input type="text" name="pcontact" >
</div></td></tr>


<tr><td><div align="left">Students contact nuber</div></td><td><div align="left">
  <input type="text" name="scontact" >
</div></td></tr>


<tr><td></td><td><input type="submit" ></td></tr>
</table>
</form>

<script type="text/javascript">
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2");
var sprytextfield3 = new Spry.Widget.ValidationTextField("sprytextfield3", "date", {format:"yyyy-mm-dd"});
var sprytextarea1 = new Spry.Widget.ValidationTextarea("sprytextarea1", {maxChars:100});
var sprytextarea2 = new Spry.Widget.ValidationTextarea("sprytextarea2", {maxChars:100});
var sprytextfield4 = new Spry.Widget.ValidationTextField("sprytextfield4", "none", {maxChars:8});
var sprytextfield5 = new Spry.Widget.ValidationTextField("sprytextfield5", "email");
</script>
<?php include("adminPart2.php"); ?>
</body>
</html>
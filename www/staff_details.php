<?php 
session_start();
if($_SESSION['status']!='admin')
{
	$_SESSION['status']='';
   header('location: login.php?typ=4');
}
 ?>			
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link rel="stylesheet" type="text/css" href="home.css" >
<link rel="stylesheet" type="text/css" href="printStyle.css" media="print"  />
<script src="SpryAssets/SpryValidationTextarea.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationTextarea.css" rel="stylesheet" type="text/css">
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">
</head>
<?php include("adminPart1.php"); ?>

<form action="add_staff.php" method="post" enctype="multipart/form-data">
<table>
<tr><td><div align="left">Id</div></td><td><div align="left"><span id="sprytextfield1">
  <input type="text" name="id" id="id">
  <span class="textfieldRequiredMsg">A value is required.</span><span class="textfieldMinCharsMsg">Minimum number of characters not met.</span></span></div></td></tr>
  
  
<tr><td><div align="left">Name </div></td><td><div align="left"><span id="sprytextfield2">
  <input type="text" name="fname" id="fname">
  <span class="textfieldRequiredMsg">A value is required.</span></span></div></td></tr>
  
  
<tr><td><div align="left">Addres</div></td><td><div align="left"><span id="sprytextarea1">
<textarea name="addrs" id="addrs" cols="30" rows="5"></textarea>
<span class="textareaRequiredMsg">A value is required.</span><span class="textareaMaxCharsMsg">Exceeded maximum number of characters.</span></span></div></td></tr>


<tr>
      <td><div align="left">Add Picture</div></td>
      <td><input type="file" name="picture" id="picture" accesskey="picture" /></td>
    </tr>


<tr><td><div align="left">contact number</div></td><td><div align="left"><span id="sprytextfield3">
  <input type="text" name="cntct" id="cntct">
  <span class="textfieldRequiredMsg">A value is required.</span><span class="textfieldInvalidFormatMsg">Invalid format.</span></span></div></td></tr>
  
  
<tr><td><td><div align="left">
  <input type="submit"  />
</div></td></td></tr></table>
</form>
<script type="text/javascript">
var sprytextarea1 = new Spry.Widget.ValidationTextarea("sprytextarea1", {maxChars:100, validateOn:["blur"]});
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1", "none", {minChars:2, validateOn:["blur"]});
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2", "none", {validateOn:["blur"]});
var sprytextfield3 = new Spry.Widget.ValidationTextField("sprytextfield3", "phone_number", {format:"phone_custom", validateOn:["blur"]});
</script>
<?php include("adminPart2.php"); ?>
</body>
</html>

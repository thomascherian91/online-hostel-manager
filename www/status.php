<?php 
session_start();
if($_SESSION['status']!='admin')
{
	$_SESSION['status']='';
   header('location: login.php?typ=4');
}
 ?>			
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script language="javascript">
function confirm_new(time)
{
	var ans=confirm("You have an already generated copy at"+time+" Do you want to replace it? ");
	if(ans==false)
	{
		alert("Process aborted...");
	}
	else if(ans==true)
	{
		window.location="saveStatus.php";
	}
}
function confirm_save()
{
		var ans=confirm("You are about to generate & store the status list for the day. Do you want to continue? ");
	if(ans==false)
	{
		alert("Process aborted...");
	}
	else if(ans==true)
	{
		window.location="saveStatus.php";
	}
}

</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Status for the day</title>
<link rel="stylesheet" type="text/css" href="home.css" >
<link rel="stylesheet" type="text/css" href="printStyle.css" media="print"  />
</head>
<?php include("adminPart1.php");
$datep=date('d,M,Y');
$date=date('Y-m-d');
$query="select * from status where date='$date'";
	$result=mysql_query($query)or die(mysql_error());
	if($row=mysql_fetch_array($result))
	{
?>
<h3><a class="linker" onclick="confirm_new('<?php echo $row['time']; ?>')">Store Status list of the day</a> <a style="text-decoration:none;" class="linker" href="getStatus.php">  &nbsp; Archives</a></h3>
<?php }
else
{
?>
<h3><a class="linker" onclick="confirm_save()">Store Status list of the day</a> <a class="linker" href="getStatus.php">  &nbsp; Archives</a></h3>
<?php } ?>
<h3>Status List for 
<?php echo $datep; ?>;
</h3></br>
<table width="801" border="1">
  <tr>
    <td width="131">User ID</td>
    <td width="121">Room No</td>
    <td width="315">Name</td>
    <td width="206">Status</td>
  </tr>
<?php
$date=date('Y-m-d');
$time=date("H:i");
$query = "select uid,name,room_no from student"; 
$result = mysql_query($query) or die(mysql_error());
while($row=mysql_fetch_array($result))
{ 
    $uid=$row['uid'];
	$query1="select * from college_go where uid='$uid' and date='$date' and not exists(select uid from college_ret where uid='$uid' and date='$date')";
	$result1=mysql_query($query1) or die(mysql_error());
	if($row1=mysql_fetch_array($result1))
	{

			
?>
		<tr>
  		<td><?php echo $row['uid']; ?></td>
   	 	<td><?php echo $row['room_no']; ?></td>
        <td><?php echo $row['name']; ?></td>
    	<td><?php echo "Went to College"; ?></td>
  		</tr>
<?php
	}
	else
	{
		$query2="select * from leave_apply where uid='$uid' and frm_date<='$date' and to_date>='$date'";
		//$query2="select * from leave_apply,leave_approve where leave_apply.uid='$uid' and leave_apply.frm_date<='$date' and leave_apply.to_date>='$date' and exists(select appl_no from leave_approve where appl_no=leave_apply.slno and status='ap') ";
		  
		  //$query2="select * from leave_apply where uid='$uid' and frm_date<='$date' and to_date>='$date' and exists(select appl_no from leave_approve where leave_approve.appl_no=leave_apply.slno and status='ap') ";
		  
		$result2=mysql_query($query2) or die(mysql_error());
		if($row2=mysql_fetch_array($result2))
		{
			$query3="select * from movement_go where uid='$uid' and date<='$date' and not exists(select movement_no from movement_ret where movement_no=movement_go.slno)";
			$result3= mysql_query($query3) or die(mysql_error());
			if($row3=mysql_fetch_array($result3))
			{
?>
				<tr>
  				<td><?php echo $row['uid']; ?></td>
   	 			<td><?php echo $row['room_no']; ?></td>
        		<td><?php echo $row['name']; ?></td>
    			<td><?php echo "Out of Hostel with Permission"; ?></td>
  				</tr>
<?php
			}
		     else
			 {	
?>
  				<tr>
  				<td><?php echo $row['uid']; ?></td>
    			<td><?php echo $row['room_no']; ?></td>
        		<td><?php echo $row['name']; ?></td>
    			<td><?php echo "Inside hostel with Permission"; ?></td>
  				</tr>
 <?php
    		}
		}
		else
		{
			$query3="select * from movement_go where uid='$uid' and date<='$date' and not exists(select movement_no from movement_ret where movement_no=movement_go.slno)";
			$result3= mysql_query($query3) or die(mysql_error());
			if($row3=mysql_fetch_array($result3))	
		//	if(isset($result3))
			{
?>
 				<tr>
  				<td><?php echo $row['uid']; ?></td>
    			<td><?php echo $row['room_no']; ?></td>
       			 <td><?php echo $row['name']; ?></td>
    			<td><?php echo "Out of hostel, without permission"; ?></td>
 	 			</tr>
<?php
    		}
			else
			{
?>
				<tr>
  				<td><?php echo $row['uid']; ?></td>
    			<td><?php echo $row['room_no']; ?></td>
        		<td><?php echo $row['name']; ?></td>
    			<td><?php echo "Inside hostel, without permission"; ?></td>
 	 			</tr>
<?php
			}
		}
	}
}
?>
</table>
<?php include("adminPart2.php"); ?>
</body>
</html>
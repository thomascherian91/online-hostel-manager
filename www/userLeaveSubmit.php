<?php 
session_start();
if($_SESSION['status']!='user')
{
	$_SESSION['status']='';
   header('location: login.php?typ=5');
}
 ?>	
 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link rel="stylesheet" type="text/css" href="home.css" >
</head>

<?php  include("userPart1.php"); 
?>
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css">


<script src="SpryAssets/SpryValidationTextarea.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationTextarea.css" rel="stylesheet" type="text/css">
<p>Leave Manager</p><form action="applyLeave.php" method="post" name="leaveapply">
<table class="form" width="754" border="0">
  <tr>
    <td width="110">From</td>
    <td width="209"><span id="sprytextfield1"><span id="sprytextfield2"><span id="sprytextfield1">
    <input value= '<?php echo date("Y-m-d"); ?>' type="text" name="fromdate" id="fromdate" accesskey="fromdate">
    <span class="textfieldRequiredMsg">A value is required.</span><span class="textfieldInvalidFormatMsg">Invalid format.</span></span></td>
    <td width="193">To<span id="sprytextfield2">
    <input value= '<?php echo date("Y-m-d"); ?>' type="text" name="todate" id="todate" accesskey="todate">
<span class="textfieldInvalidFormatMsg">Invalid format.</span></span></td>
    <td width="224">&nbsp;</td>
  </tr>
  <tr>
 <?php //   <td>Half Day  <input name="halfday" type="checkbox" id="halfday" accesskey="halfday" value="yes" /></td> ?>
    <td>Morning 
      <label>
        <input type="checkbox" name="morning" id="morning" accesskey="morning" value="yes" />
      </label></td>
    <td>Afternoon 
      <label>
        <input type="checkbox" name="afternoon" id="afternoon" accesskey="afternoon" value="yes" />
      </label></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Reason</td>
    <td colspan="3"><span id="sprytextarea1"><span id="sprytextarea1">
      <textarea name="reason" id="reason" cols="45" rows="5" accesskey="reason"></textarea>
      <span class="textareaRequiredMsg">A value is required.</span></span></td>
    </tr>
  <tr>
    <td>No of Working days</td>
    <td><span id="sprytextfield3">
    <input type="text" name="days" id="days" accesskey="days">
    <span class="textfieldRequiredMsg">A value is required.</span><span class="textfieldInvalidFormatMsg">Invalid format.</span></span></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><label>
      <input type="submit" name="submit" id="submit" value="Submit" accesskey="submit" />
    </label></td>
    <td>  <input type="reset" name="reset" id="reset" value="Reset" accesskey="submit" /></td>
  </tr>
</table></form>
<p>&nbsp;</p>



<h3>Status of Leaves taken so far</h3></br><form action="processLeave.php" method="post">
<table width="1000" border="1">
  <tr>
    <td width="108">User ID</td>
    <td width="115">Application Date</td>
    <td width="112">From</td>
    <td width="112">To</td>
    <td width="147">No. of Working days</td>
    <td width="307">Reason</td>
    <td width="69">Status</td>
  </tr>
<?php
$uid=$_SESSION['user'];
$query="select * from leave_apply where uid='$uid' order by slno desc";
$result = mysql_query($query) or die(mysql_error());
while($row=mysql_fetch_array($result))
{ 
	$apno=$row['slno'];
	$query2="select * from leave_approve where appl_no='$apno'";
	$result2= mysql_query($query2) or die(mysql_error());
	if($row2=mysql_fetch_array($result2))
	{
		if($row2['status']=='ap')
			$status='Approved';
		else if($row2['status']=='re')
			$status='Rejected';
	}
	else
		$status='Pending';
?>
  <tr>
    <td><?php echo $row['uid']; ?></td>
    <td><?php echo $row['app_date']; ?></td>
    <td><?php echo $row['frm_date']; ?></td>
    <td><?php 
	$from=$row['to_date'];
	if($from=='0000-00-01') {echo "halfday-fn";}
	elseif($from=='0000-00-02'){echo "halfday-an";}
	elseif($from=='0000-00-00'){echo $row['frm_date'];}
	else {echo $from; } ?></td>
    <td><?php echo $row['workingdays_no']; ?></td>
    <td><?php echo $row['reason']; ?></td>
    <td><?php echo $status; ?></td>
  </tr>
 <?php
}
?>
</table>


<?php include("userPart2.php"); ?>

<script type="text/javascript">
<!--
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1", "date", {format:"yyyy-mm-dd", hint:"yyyy-mm-dd format", validateOn:["blur"]});
var sprytextarea1 = new Spry.Widget.ValidationTextarea("sprytextarea1", {validateOn:["blur"], hint:"Type the reason here..."});
var sprytextfield3 = new Spry.Widget.ValidationTextField("sprytextfield3", "integer", {validateOn:["blur"]});
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2", "date", {format:"yyyy-mm-dd", hint:"yyyy-mm-dd format", validateOn:["blur"], isRequired:false});
//-->
</script>
</body>
</html>
